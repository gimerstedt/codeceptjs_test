Feature('Kommentera historik');

Scenario('latsaskommentera lite...', (I) => {
  I.amOnPage('http://kustbevakningen.se/');
  I.click('A-Ö lista');
  I.click('Historik');
  I.see('strandridare...');
  I.dontSee('Kontrollord');
  I.click('Kommentera');
  I.see('Kontrollord');
  I.fillField('creator', 'Patrik');
  I.fillField('email', 'patrik@dev.null');
  I.fillField('comment', '12345:q!');
  I.see('992 tecken kvar');
  I.click('cancel');
  I.wait('2');
  I.dontSee('Kontrollord');
  I.seeInSource('episerver');
});
