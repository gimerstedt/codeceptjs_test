Feature('Sokning pa kbv.se');

Scenario('testar lite sokning', (I) => {
  I.amOnPage('http://kustbevakningen.se');
  I.fillField('q', 'fartyg');
  I.click('Sök');
  I.seeInTitle('Sök');
  I.haveHeader('Server', 'Microsoft-IIS/8.5');
  I.see('Fartygskatalog');
  I.click('Fartygskatalog');
  I.see('Bilder på Kustbevakningens fartyg');
});
